import { combineReducers } from 'redux'

import results from './results'
import details from './details'


const movieApp = combineReducers({
  details,
  results,
})

export default movieApp