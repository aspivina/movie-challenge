import  { 
    IS_DETAIL_FETCHING,
    SET_DETAIL_RESPONSE,
    SET_DETAIL_ERROR,
//    SET_IMDBID,
    SET_MOVIEDETAIL,        
} from './constants';

const initialState = {
    isFetching : false,     //LOADING animation
    detailResponse: false,
    detailError: null,      // it only shows an error when the fetch failed for any reason
   // imdbId: null,
    movieDetail: null,        // movie detail object
};
const state = (state = initialState, {type, payload}) => {
    switch(type) {
        case IS_DETAIL_FETCHING:
            return{
            ...state,
            isFetching: payload,
        }
        case SET_DETAIL_RESPONSE:
            return{
            ...state,
            detailResponse: payload,
            }
        case SET_DETAIL_ERROR:
            return{
            ...state,
            detailError: payload,
            }
        // case SET_IMDBID:
        //     return{
        //     ...state,
        //     imdbId: payload,
        //     }
        case SET_MOVIEDETAIL:
        console.log("______________________________________________________");
        console.log(type,": ",payload);
            return{
            ...state,
            movieDetail: payload,
            }
        default:
            return state
    }
}
export default state;




