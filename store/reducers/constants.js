export const IS_FETCHING            ='IS_FETCHING';
export const SET_RESPONSE           ='SET_RESPONSE';
export const SET_RESPONSE_SUCCEEDED ='SET_RESPONSE_SUCCEEDED';
export const SET_RESPONSE_FAILED    ='SET_RESPONSE_FAILED';
export const SET_MOVIELIST          ='SET_MOVIELIST';
export const SET_FILTERFLAG         ='SET_FILTERFLAG';
export const SET_ERROR              ='SET_ERROR';

export const SET_IMDBID             ='SET_IMDBID';
export const SET_CURRENTPAGE        ='SET_CURRENTPAGE';
export const SET_TOTALRESULTS       ='SET_TOTALRESULTS';
export const SET_TOTALPAGES         ='SET_TOTALPAGES';
export const SET_PAGELIST           ='SET_PAGELIST';
export const SET_TERM               ='SET_TERM';
// export const SET_YEAR =             'SET_YEAR';
export const SET_TYPE =             'SET_TYPE';


// //related to DETAIL fetching
export const IS_DETAIL_FETCHING =   'IS_DETAIL_FETCHING';
export const SET_DETAIL_RESPONSE =  'SET_DETAIL_RESPONSE';
export const SET_DETAIL_ERROR =     'SET_DETAIL_ERROR';
export const SET_MOVIEDETAIL =      'SET_MOVIEDETAIL';




