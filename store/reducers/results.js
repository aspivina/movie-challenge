import  { IS_FETCHING,
          SET_RESPONSE,
          SET_ERROR,
          SET_MOVIELIST,
          SET_FILTERFLAG,
          SET_CURRENTPAGE,
          SET_TOTALRESULTS,
          SET_TOTALPAGES,
          SET_PAGELIST,
//          SET_IMDBID,
          SET_TERM,
          SET_YEAR,
          SET_TYPE,
          } from './constants';
const initialState = {
  isFetching : false, //either for movies or movie detail //LOADING animation
  response: false,    //it only shows TRUE when the fetch succeeded
  error: null,        // it only shows an error when the fetch failed for any reason
  totalResults: 0,    // number
  totalPages: 0,
  pageList:[],
  movieList: [],      // collection of movie objects
  currentpage: 1,
//  imdbId: null,
  filterFlag: false,  // Are search filters ON or OFF ?
  term: '',           //search term
  year: '',           // year filter
  type: 'all',        // type filter
};
const state = (state = initialState, {type, payload}) => {
  switch(type) {
    case IS_FETCHING:
      return{
        ...state,
        isFetching: payload,
      }
    case SET_RESPONSE:

      return {
        ...state,
        response : payload,
      }
    case SET_TERM:
      return {
        ...state,
        term : payload,
      }
    case SET_TYPE:
      return {
        ...state,
        type : payload,
      }
    case SET_ERROR:
      return {
        ...state,
        error : payload,
      }
    case SET_CURRENTPAGE:
      return {
        ...state,
        currentpage : payload,
      }
    case SET_TOTALRESULTS:
      return {
        ...state,
        totalResults : payload,
      }
    case SET_TOTALPAGES:
      return {
        ...state,
        totalPages : payload,
      }
    case SET_PAGELIST:
      return {
        ...state,
        pageList : payload,
      }
    case SET_MOVIELIST:
      return{
        ...state,
        movieList: payload,
      }
    case SET_FILTERFLAG:
      return{
        ...state,
        filterFlag: !state.filterFlag,
      }
    default:
      return state
  }
}
export default state;




