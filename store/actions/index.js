import  { IS_FETCHING,
          SET_RESPONSE,
          SET_MOVIELIST,
          SET_FILTERFLAG,
          SET_TYPE,
          SET_TERM,
          SET_IMDBID,
          SET_ERROR,
          SET_TOTALRESULTS,
          SET_TOTALPAGES,
          SET_PAGELIST,
          IS_DETAIL_FETCHING,
          SET_MOVIEDETAIL,        
          SET_DETAIL_RESPONSE,
          SET_DETAIL_ERROR,   


  } from '../reducers/constants';

import getMovies from '../api/'  

// _____________________________________________________________
const setIsFetching         = (payload) => ({type: IS_FETCHING,payload});
// _____________________________________________________________
const setResponse           = (payload) => ({type: SET_RESPONSE,payload});
// _____________________________________________________________
const setError              = (payload) => ({type: SET_ERROR,payload});
// _____________________________________________________________
export const setFilterFlag  = () => ({type: SET_FILTERFLAG});
// _____________________________________________________________
export const setType  = (payload) => ({type: SET_TYPE,payload});
// _____________________________________________________________
export const setTerm  = (payload) => ({type: SET_TERM,payload});
// _____________________________________________________________
const setImdbId             = (payload) => {{type: SET_IMDBID, payload}};
// _____________________________________________________________
const setMovieList          = (payload) => ({type: SET_MOVIELIST, payload});
// _____________________________________________________________
const setTotalResults         = (payload) => ({type: SET_TOTALRESULTS, payload});
// _____________________________________________________________
const setTotalPages          = (payload) => ({type: SET_TOTALPAGES, payload});
// _____________________________________________________________
const setPageList          = (payload) => ({type: SET_PAGELIST, payload});

//DETAIL actions
// _____________________________________________________________
const setDetailIsFetching   = (payload) => ({type: IS_DETAIL_FETCHING,payload});
// _____________________________________________________________
const setMovieDetail        = (payload) => ({type: SET_MOVIEDETAIL, payload});
// _____________________________________________________________
const setResponseDetail     = (payload) => ({type: SET_DETAIL_RESPONSE, payload});
// _____________________________________________________________
const setErrorDetail        = (payload) => ({type: SET_DETAIL_ERROR, payload});
// _____________________________________________________________

export const fetchMovies = ()=>{
  return(dispatch, getState )=>{
    dispatch(setIsFetching(true)); // Turns on the loading animation
    console.log("_____________ _______________ ____________________");
    console.log("term=",getState().results.term+"&type="+getState().results.type);
    let myTerm = "";
    if(getState().results.type != "all"){
      myTerm="s="+getState().results.term+"&type="+getState().results.type;
    }else{
      myTerm="s="+getState().results.term;
    }
    console.log(myTerm);
    getMovies(myTerm).then(data => {
    dispatch(setIsFetching(false)); // Turns on the loading animation
    // console.log("after fetch",data.Search)
    if (data.Response === "True") {
      //console.log("after fetch",data.Search.Title)
      let tp =Math.ceil(parseInt(data.totalResults)/10);
      dispatch(setMovieList(data.Search)); // 
      dispatch(setResponse(true)); // 
      dispatch(setTotalResults(data.totalResults)); // 
      dispatch(setTotalPages(tp)); // 
      dispatch(setPageList(Array.from({length: tp}, (v, i) => i+1))); // 
      dispatch(setError(null)); // 
    }else{
      dispatch(setMovieList([])); // 
      dispatch(setResponse(false)); // 
      dispatch(setTotalResults(0)); // 
      dispatch(setTotalPages(0)); // 
      dispatch(setPageList([])); // 
      dispatch(setError(data.Error )); // 
    }
  });
  }
};
// _____________________________________________________________
export const fetchMovieDetails = (term)=>{
  return(dispatch)=>{
    dispatch(setDetailIsFetching(true)); // Turns on the loading animation
    getMovies(term).then(data => {
      dispatch(setDetailIsFetching(false)); // Turns on the loading animation
      dispatch(setMovieDetail(null)); // 

      if (data.Response === "True") {
        console.log("after DETAIL fetch",data)
        dispatch(setMovieDetail(data)); // 
        dispatch(setResponseDetail(true));
      }else{
        dispatch(setResponseDetail(false));
        dispatch(setErrorDetail(data.Error ));
      }
    });
  }
};



