const originalURL = 'https://www.omdbapi.com/?:TERM&apikey=914e9371';

export default async function getMovies(term) {
  const url = originalURL.replace(':TERM', term);
  // const proxyUrl = 'https://cors-anywhere.herokuapp.com/';
  // const res = await fetch(proxyUrl+url);
  try {
    const res = await fetch(url);
    const json = await res.json();
    return json;
  }
  catch (e) {
    return { "Error": "Fatal Error 403" }
  }

}