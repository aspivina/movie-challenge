import React, {useState} from 'react'
import PropTypes from 'prop-types'
import { Image, TouchableOpacity, Text, View } from 'react-native';
import styles from './styles'
import { withNavigation } from 'react-navigation';

const MovieItem = ({ movie,navigation }) => {
  const [image,imgLinkError] = useState( {uri: movie.Poster });
  //console.log("movie title: ", image);
  const onImgLinkError = (id) => {
  //   imgLinkError({uri:'https://i.imgur.com/H3IKEb6.png'});
      imgLinkError(require('../../assets/image-error.png'));
  }
  return (
    <View style={styles.container}>
      <TouchableOpacity 
              style={ styles.movieBox} 
              onPress={() => navigation.navigate('Details', {
                              id: movie.imdbID
                      })} 
              activeOpacity={.7}>
        <View style={styles.movieBox__img}>
          <Image
                style={styles.movieBox__imgImg}
                source={image}
                onError={()=>onImgLinkError()}
          />
        </View>
        <Text 
                style={styles.movieBox__title}
                numberOfLines={1}>
          {movie.Title}
        </Text>
        <Text 
                style={styles.movieBox__title}>
          {movie.Year}
        </Text>
      </TouchableOpacity>
    </View>
  );
}
  // MovieItem.propTypes = {
    //   todo: PropTypes.shape({
    //             id: PropTypes.number.isRequired,
    //             completed: PropTypes.bool.isRequired,
    //             text: PropTypes.string.isRequired
    //         }).isRequired,
    //   onClick: PropTypes.func.isRequired,
    //   onDelete: PropTypes.func.isRequired
  // }
export default withNavigation( MovieItem)