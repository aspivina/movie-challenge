import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex:1,
      width: 120,
      height : 220,

      flexDirection: 'row',
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'space-between',
      justifyContent: 'center',
  //    backgroundColor: 'yellow',
      borderRadius: 10,
    },
    movieBox: {
      flex:1,
      flexDirection: 'column',
       alignItems: 'center',
        alignContent: 'space-between',
        //justifyContent: 'space-between',
         justifyContent: 'center',
      height : 220,
     // width: 120,
     // backgroundColor: 'brown',
      color: 'white',
      borderRadius: 10,

      
    },
    movieBox__img: {
      flex:1,
      flexDirection: 'column',
      alignSelf: 'stretch',
      height: 182,
      width: 120,
      // height: '100%',
      
    },
    movieBox__imgImg: {
       flex:1,
      //  flexDirection: 'column',
      alignSelf: 'center',
      // width: 120,
      width: 120,
      maxHeight: 182,
      borderRadius: 10,

    },

    movieBox__title:{
      color: 'white',
      fontSize: 12,
      paddingRight: 5,  
      paddingLeft: 5,  
      height: 16,
    },

   
  
  });
  export default styles;