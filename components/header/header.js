import React,{useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import { TouchableOpacity, TextInput, Image, View, Text } from 'react-native';
import styles from './styles';

import Filter from '../filter/filter'
const triangleUp = require('../../assets/triangle-up.png');
const triangleDown = require('../../assets/triangle-down.png');

const Header = ({ filterFlag,term,year,type, onSearch, settingTerm, settingfilterFlag,settingFilterType}) => {

   const opening = ["wolf","crazy","baby","superman","elite","murder","future"];
  //const opening = ["Super man","Super man","Super man","Super man","Super man","Super man","Super man"];
  let rd = Math.floor((Math.random()*7));
  const [value,setValue] = useState(opening[rd]);
  //sets the first term as a random word from the -openin- array
  useEffect(()=>{  settingTerm(value)},[]);
  useEffect(() => {onSearch()},[])
 
  const onChangeType =(option)=>{
    console.log("my actual term: "+term+" and my option was: ", option);
    settingFilterType(option);
    onSearch();

  }  
  const keyUpHandler = (v) => {
      setValue(v);
  }
  const onClickAdd = () => {
      // hit enter, create new item if field isn't empty
      if(value){
        settingTerm(value);
        onSearch();
        // setValue(null);
      }
    } 
const handleToggleClick = () => {
    settingfilterFlag();
    console.log(filterFlag);

    }  

  let input;

  return (
    <View style={styles.headbar}>
        <Image
          style={styles.headbar__logo}
          source={require('../../assets/avalith-logo.png')}
        />
        <View style={styles.headbar__input}>
          <TouchableOpacity style={{margin: 5}} onPress={() => onClickAdd()}>
            <Image
            style={{width: 20, height: 20}}
            source={require('../../assets/search.png')}
          />
          </TouchableOpacity>
          <TextInput
            autoFocus={true}
            style={styles.input}
            placeholder="Search Movie"
            onChangeText={(v) => keyUpHandler(v)}
            value={value}
            clearButtonMode="always"
          />
        </View>
        <View style={styles.headbar__advanceSearch}>
          <Filter filterFlag={filterFlag} onChange={(e)=>(onChangeType(e))} />          

          <TouchableOpacity style={{margin: 5}} onPress={() => handleToggleClick()}>
            {filterFlag ? (
              <View style={styles.headbar__filter}>
                <Text style={styles.filterText}>
                  Filters
                </Text>
                <Image
                  style={styles.imgFilter}
                  source={triangleUp}
                />
              </View>
            ):(
              <View style={styles.headbar__filter}>
                <Text style={styles.filterText}>
                  Filters
                </Text>                
                <Image
                  style={styles.imgFilter}
                  source={triangleDown}
                />
              </View>
            )}                
        </TouchableOpacity>          

        </View>

    </View>
  );
}

// MovieList.propTypes = {
//   todos: PropTypes.arrayOf(
//     PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       completed: PropTypes.bool.isRequired,
//       text: PropTypes.string.isRequired
//     }).isRequired
//   ).isRequired,
//   onClick: PropTypes.func.isRequired
// }

export default Header