import React from 'react';
import { connect} from 'react-redux';
import {setTerm,setType, fetchMovies, setFilterFlag} from '../../store/actions' 


import Header from './header'

const mapStateToProps = (state) => {
  console.log('State filterFlag', state.results.filterFlag)
  return {
    //state.results is the reducer in use (-results-)
    filterFlag :  state.results.filterFlag,
    term :  state.results.term,
    year :  state.results.year,
    type :  state.results.type,
  }
}
const mapDispatchToProps = (dispatch) => {
    return {
      onSearch: ()                => {dispatch(fetchMovies())},
      settingTerm: (value)        => {dispatch(setTerm(value))},
      settingFilterType: (type)   => {dispatch(setType(type))},
      settingfilterFlag: (flag)   => {dispatch(setFilterFlag(flag))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Header);







