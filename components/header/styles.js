import { StyleSheet } from 'react-native';
import colors from '../../assets/colors';

const styles = StyleSheet.create({
    headbar: {
        height: 120,
        flexDirection: 'column',
        backgroundColor: colors.bgDark,
        justifyContent: 'space-between',
        alignContent: 'center',
         alignItems: 'center',
         marginTop: 20,
      },
      headbar__logo: {
        width: 130, 
        height: 30,
        margin: 3,
      },
      headbar__input:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: 30,
        backgroundColor: colors.bgDarkContrast,
        borderRadius: 10,
        width: 300,
        margin: 10,
      },
      input:{
        height: 40,
        width: 250,
        backgroundColor: colors.bgDarkContrast,
        color: 'white',
        height: 30,
        padding: 5,
        borderRadius: 5,
      },
      headbar__advanceSearch:{
        flex: 1,
        height: 30,
        flexDirection: 'row',

        alignItems: 'center',
        alignContent: 'flex-start',
        justifyContent: 'center',

      },
      headbar__filter:{
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        color: colors.mainColor,
        margin: 10,
        height: 30,
      },
      filterText:{
        color: colors.mainColor,
      },
      imgFilter: {
          height : 10,
          width: 12,
          marginLeft: 5,
      },
              // .imgFilter {
        //   height: 10px;
        //   fill: grey;
        //   padding: 0 15px;
        //   cursor: pointer;
        // },
  });
  export default styles;