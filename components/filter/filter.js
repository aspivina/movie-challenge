import React,{useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import { Picker,TouchableOpacity, TextInput, Image, View, Text } from 'react-native';

import styles from './styles';


const Filter = ({ filterFlag, onChange}) => {
  const [type,setValue] = useState("all");

  const  handleSelectOnChange=(e)=>{
      console.log("select value: ",e);
      setValue(e);
      onChange(e);

      // this.setState({typeValue: e.target.value}, ()=>{
      //     if (this.validateSearch(this.state.value)){
      //       let newTerm =   this.validateSearch(this.state.value)
      //                     + this.validateYear(this.state.yearValue)
      //                     + this.validateSelect(this.state.typeValue);
      //       console.log("New term: ", newTerm);
      //       this.props.freshTerm(true);
      //       this.props.callback(newTerm);                              
      //     }
      // });
    }

  return (
    <View style={styles.headbar__filter}>
        {filterFlag && 
          <View style={styles.headbar__input}>
            <View style={styles.headbar__picker}>
              <Text style={styles.filterText}>
                Type:
              </Text>  
              <Picker
                style={styles.pickerOption}
                itemStyle={styles.itemPickerOption}
                selectedValue={type}
                onValueChange={(itemValue, itemIndex) => (handleSelectOnChange(itemValue))
                }>
                <Picker.Item  itemTextStyle={{fontSize: 12}} label="all" value="all" />
               
                <Picker.Item label="movie" value="movie" />
                <Picker.Item label="series" value="series" />
                <Picker.Item label="episodes" value="episodes" />
              </Picker>
              {/* <Text style={styles.filterText}>
                  Year
                </Text>            
                <TextInput
                  style={styles.input}
                  placeholder="year"
                  onChangeText={(v) => keyUpHandler(v)}
                  value={value}
                  clearButtonMode="always"
                /> */}
            </View>
          </View>
        }

    </View>
  );
}

// MovieList.propTypes = {
//   todos: PropTypes.arrayOf(
//     PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       completed: PropTypes.bool.isRequired,
//       text: PropTypes.string.isRequired
//     }).isRequired
//   ).isRequired,
//   onClick: PropTypes.func.isRequired
// }

export default Filter