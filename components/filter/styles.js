import { StyleSheet } from 'react-native';
import colors from '../../assets/colors';

const styles = StyleSheet.create({

    headbar: {
        height: 120,
        flexDirection: 'column',
        backgroundColor: colors.bgDark,
        justifyContent: 'space-between',
        alignContent: 'center',
         alignItems: 'center',
         marginTop: 20,
         
      },


      headbar__filter:{
        flexDirection: 'row',
        alignItems: 'center',
        color: colors.mainColor,
        margin: 10,
      },
      headbar__picker:{
        flexDirection: 'row',
        alignItems: 'center',
        color: colors.mainColor,
        
        margin: 10,
      },
      filterText:{
        color: colors.mainColor,
        
      },
      pickerOption:{
        backgroundColor : 'green',
        backgroundColor: colors.bgDarkContrast,
        color: 'white',
        height : 25,
        width: 60,
        borderRadius: 15,
        fontSize: 10,
        marginLeft: 10,


                //   background-color: var(--main-bgDarkContrast-color);

      },
      itemPickerOption:{
        backgroundColor : 'red',
        color: 'white',
        height : 20,
        width: 50,
        borderRadius: 15,


                //   background-color: var(--main-bgDarkContrast-color);

      },

       // .select-option {
        //   height: 25px;
        //   font-size: 12px;
        //   background-color: var(--main-bgDarkContrast-color);
        //   color:  var(--main-color);
        //   width: 75px;
        //   border-radius: 5px;
        //   border: none;
        // },


        // .headbar__filter {
        //   display: flex;
        //   flex-direction: row;
        //   margin: 0 60px;
        //   align-items: center;
        //   font-size: 18px;
        //   color:  var(--main-color);
        //   width: 100px;
        // },



        // .input--year {
        //   height: 25px;
        //   border-radius: 5px;
        //   width: 50px;
        //   font-size: 14px;
        //   color:  var(--main-color);
        //   margin-right: 10px;
        // },
        // .input:focus {
        //   outline: none;
        // },

        // .imgFilter {
        //   height: 10px;
        //   fill: grey;
        //   padding: 0 15px;
        //   cursor: pointer;
        // },
        // .filter--disabled {
        //   display: none;
        // },
        // .button {
        //   width: 60px;
        //   border-radius: 10px;
        //   height: 25px;
        // },
        // .headbar__advanceSearch {
        //   display: flex;
        //   flex-direction: row;
        //   justify-content: space-between;
        //   align-content: right;
        //   align-items: center;
        // },
        // .advanceSearch-filter {
        //   display: flex;
        //   flex-direction: row;
        //   justify-content: space-between;
        //   align-items: center;
        //   width: 220px;
        // },
        // .filter-option {
        //   display: flex;
        //   flex-direction: row;
        //   align-items: center;
        //   width: 100px;
        //   color:  var(--main-color);
        // },
        // .label {
        //   margin: 0 10px 0 15px;
        //   font-size: 16px;
        // },
        // .label p {
        //   margin: 0;
        // },
 








  
  });
  export default styles;