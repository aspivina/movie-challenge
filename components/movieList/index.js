import React from 'react'
import PropTypes from 'prop-types'

import { StyleSheet, ScrollView, View, Text } from 'react-native';

import MovieItem from '../movieItem'
import styles from './styles'
const MovieList = ({ movies, response}) => {
  return (
    <View style={styles.container1}>
        <ScrollView style={styles.container} >
          <View  style={styles.movieContent} >

          {movies.map((item,index) => (
            <View style={styles.movies} key={index+item.imdbID} >
              {response &&
                <MovieItem 
                  movie={item}
                />
              }
            </View>
          ))}
          </View>
        </ScrollView>
    </View>

  );
}

// MovieList.propTypes = {
//   todos: PropTypes.arrayOf(
//     PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       completed: PropTypes.bool.isRequired,
//       text: PropTypes.string.isRequired
//     }).isRequired
//   ).isRequired,
//   onClick: PropTypes.func.isRequired
// }

export default MovieList