import { StyleSheet } from 'react-native';
import colors from '../../assets/colors';

const styles = StyleSheet.create({
  container1: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.bgDarkContrast,
    justifyContent:'flex-start',
  },
    container: {
       margin: 10,
    },
    movieContent:{
      flexDirection: 'row', //ROW!!!
      flexWrap: 'wrap',
      justifyContent: 'space-evenly',
      margin: 10,
    },
    movies:{
      flex:1,
      flexBasis: 120,
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      margin: 10,
      height: 220,
      width: 120,
    },  
  });
  export default styles;