import React from 'react';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk'

import movieApp from './store/reducers'
let store = createStore(
  movieApp,
  applyMiddleware(thunk),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)
// NAVIGATOR
import AppNavigator from './screens/appNavigator'
import { createAppContainer } from 'react-navigation';
const AppContainer = createAppContainer(AppNavigator);

export default function App() {
  return (
     <Provider store={store}>
         <AppContainer />
     </Provider> 
  );
}
