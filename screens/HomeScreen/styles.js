import { StyleSheet } from 'react-native';
import colors from '../../assets/colors';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bgDark,
        alignItems: 'center',
        alignContent: 'space-between',
      },
      title: {
        fontSize: 19,
        fontWeight: 'bold',
        color: 'white',
       // backgroundColor: colors.contrastColor,
    },
  });

  export default styles;  