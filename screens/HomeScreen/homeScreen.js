import React,{useEffect} from 'react';
import { ActivityIndicator,Text, View } from 'react-native';
import styles from './styles'

import Header from '../../components/header'
import MovieList from '../../components/movieList'
import colors from '../../assets/colors';

const HomeScreen = ({movies,error, response,isFetching,onfetchMovies}) => {
    return (
       <View style={styles.container}>
          <Header/>
           {isFetching &&
              <ActivityIndicator size="large" color={colors.contrastColor} />
           }
           {response ?(
               <MovieList movies={movies} response={response} />
            ):(
               <Text style={styles.title}>{error}</Text>
            )
           }
        </View>
    )
};
export default (HomeScreen);