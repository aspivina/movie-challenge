import React from 'react';
import { connect, useSelector} from 'react-redux';
import { fetchMovies} from '../../store/actions' 

import HomeScreen from './homeScreen'

const mapStateToProps = (state) => {
 console.log('State', state.results)
  return {
  movies :  state.results.movieList,
  isFetching :  state.results.isFetching,
  error :  state.results.error,
  // movies : useSelector(state => state.movieList),
  response : state.results.response,
}
}
const mapDispatchToProps = (dispatch) => {
    return {
      onfetchMovies: (term) => { dispatch(fetchMovies(term))}
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);







