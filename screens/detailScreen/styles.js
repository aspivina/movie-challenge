import { StyleSheet } from 'react-native';
import colors from '../../assets/colors';

const styles = StyleSheet.create({
    detailContent: {
      flex: 1,
      backgroundColor: colors.bgDarkContrast,
      alignItems: 'center',
      alignContent: 'space-between',
      padding : 10,
    },
    container: {
      flex: 1,
      width: '100%',
      alignContent: 'center',
      backgroundColor: colors.bgDarkContrast,
    },
    content: {
      flex: 1,
      width: '100%',
      alignContent: 'center',
      justifyContent: 'center',
      alignItems: 'center',
    },

    movieBox__img: {
      flex:1,
      height: 437 ,
      width: 250 ,
    },
    movieBox__imgImg: {
       flex:1,
      //  flexDirection: 'column',
      alignSelf: 'center',
      // width: 120,
      height: 437 ,
      width: 250 ,
      borderRadius: 10,

    },

    movieBox__title:{
      color: colors.mainColor,
      fontSize: 18,
      paddingRight: 5,  
      paddingLeft: 5,  
      height: 25,
      margin:5,

    },
    movieBox__metaData:{
      color: colors.mainColor,
      fontSize: 12,
      paddingRight: 5,  
      paddingLeft: 5,  
      height: 16,
      margin:5,

    },
    bodyPlot:{
      flex: 1,
      flexWrap: 'wrap',

    },
    movieBox__bodyPlot:{
        color: colors.mainColor,
        fontSize: 12,
        paddingRight: 5,  
        paddingLeft: 5,  
        // height: 300,
        marginRight:10,
        marginLeft:10,
        textAlign:'justify',
    }
  });
  export default styles;