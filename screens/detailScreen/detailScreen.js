import React, { useEffect, useState } from "react";
import { ScrollView, ActivityIndicator, Image, View, Text } from "react-native";
import styles from "./styles";
import colors from "../../assets/colors";

import { withNavigation } from "react-navigation";

const imageError = require("../../assets/image-error.png");
const DetailScreen = ({
  movieDetail,
  detailError,
  detailResponse,
  isFetching,
  onSearch,
  navigation,
  navigationOptions
}) => {
  useEffect(() => {
    onSearch(termId);
  }, []);

  const termId = navigation.state.params.id;
  return (
    <View style={styles.detailContent}>
      {/* <Text style={styles.active}>{navigation.state.params.id}</Text> */}
      {/* <Text >{detailResponse}</Text> */}
      {isFetching && (
        <ActivityIndicator size="large" color={colors.contrastColor} />
      )}
      {movieDetail && (
        <ScrollView style={styles.container}>
          {detailResponse && !isFetching ? (
            <View style={styles.content} >
                <View style={styles.movieBox__img}>
                  {movieDetail && !(movieDetail.Poster === "N/A") ? (
                    <Image
                      style={styles.movieBox__imgImg}
                      source={{ uri: movieDetail.Poster }}
                      // source={image}
                    />
                  ) : (
                    <Image
                      style={styles.movieBox__imgImg}
                      // source={{uri:movieDetail.Poster}}
                      source={imageError}
                    />
                  )}
                </View>
                <Text style={styles.movieBox__title} numberOfLines={2}>
                  {movieDetail.Title}
                </Text>
                <Text style={styles.movieBox__metaData}>
                  {movieDetail.imdbRating}/10
                </Text>
                <Text style={styles.movieBox__metaData}>
                  {movieDetail.Released}
                </Text>
                <Text style={styles.movieBox__metaData}>
                  {movieDetail.Genre}
                </Text>

                <ScrollView style={styles.bodyPlot}>
                  <Text style={styles.movieBox__bodyPlot}
                    >{movieDetail.Plot}</Text>
                </ScrollView>
            </View>
          ) : (
            <View style={styles.content}>
              <Text style={styles.title}>{detailError}</Text>
            </View>
          )}
          </ScrollView>
      )}
    </View>
  );
};
//export default DetailScreen;
export default withNavigation(DetailScreen);
