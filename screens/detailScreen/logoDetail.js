import React from 'react';
import { Image, View, Text } from 'react-native';

const LogoTitle = ()=>{
    return (
        <View style={{ height: 30,
            flex:1,
            flexDirection: "row",
            alignContent: "center",
            justifyContent: "center",
            alignItems: 'center' }}>
          <Image
            source={require('../../assets/avalith-logo.png')}
            style={{ width: 130, height: 30,marginLeft: -50, 
             }}
          />
        </View>
    )
};
export default LogoTitle;