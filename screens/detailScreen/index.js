import React from 'react';
import { connect} from 'react-redux';
import { fetchMovieDetails} from '../../store/actions' 

import DetailScreen from './detailScreen'

const mapStateToProps = (state) => {
  console.log('State details title: ', state.details)

  return {
    //state.results is the reducer in use (-details-)
    detailResponse :  state.details.detailResponse,
    movieDetail: state.details.movieDetail,
    detailError: state.details.detailError,
    isFetching: state.details.isFetching,
  }
}
const mapDispatchToProps = (dispatch) => {
    return {
      onSearch: (term) => { dispatch(fetchMovieDetails("i="+term))},
    }
    
}
export default connect(mapStateToProps,mapDispatchToProps)(DetailScreen);
