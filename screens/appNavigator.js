import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './homeScreen'
import DetailsScreen from './detailScreen'
import LogoTitle from './detailScreen/logoDetail'

// import colors from '../../assets/colors';
const AppNavigator = createStackNavigator(
    {
      Home: { screen: HomeScreen,
        navigationOptions: () => ({
          // headerMode : 'none',
          headerTransparent : true,
          }),
      },
       Details: { screen: DetailsScreen,
         navigationOptions: () => ({
           //title: 'Details',
           path: 'detail/:id',
           headerStyle: {
             backgroundColor:  '#1a1a1e',
             // backgroundColor:  colors.bgDark,
           },
           headerTitle: () => <LogoTitle />,
           headerTintColor: 'white',
           headerTitleStyle: {
             fontWeight: 'bold',
           },
           }),     
       },
    },
  );

export default AppNavigator;
