const colors = {
    mainColor: '#aaaaaa',
    bgDark: '#1a1a1e',
    bgDarkContrast: '#212126',
    contrastColor: '#e61e3d',
  
  };
  export default colors ;